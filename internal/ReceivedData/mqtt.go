package ReceivedData

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/maoyuting0503109/fraework/databases"
	"gitee.com/maoyuting0503109/fraework/emqx"
	"gitee.com/zhangsan7774/data-centers/consts"
	"gitee.com/zhangsan7774/data-centers/internal/config"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"go.mongodb.org/mongo-driver/bson"
	"log"
)

func ReceivedData(c config.Config, client mqtt.Client) {
	fmt.Println(c)
	info := emqx.Subscribe(client, 0, "Medical")

	for {
		select {
		case d := <-info:
			var data map[string]interface{}
			err := json.Unmarshal([]byte(d), &data)

			log.Println("消息接收到了", d)

			bsonData := bson.M(data)

			err = databases.InstallOne(context.Background(), bsonData, consts.Address)
			if err != nil {
				log.Println("Mongodb存储失败：", err.Error())
			}
		}
	}

}
