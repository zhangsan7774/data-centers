package config

import "github.com/zeromicro/go-zero/zrpc"

type Config struct {
	zrpc.RpcServerConf
	Mqtt Emqx `yaml:"Mqtt"`
}

type Emqx struct {
	Qos   int    `yaml:"Qos"`
	Topic string `yaml:"Topic"`
}
