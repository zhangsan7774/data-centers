package logic

import (
	"context"
	"encoding/json"
	"gitee.com/maoyuting0503109/fraework/databases"
	"gitee.com/zhangsan7774/data-centers/consts"
	"gitee.com/zhangsan7774/data-centers/dataCenter"
	"gitee.com/zhangsan7774/data-centers/internal/svc"
	"go.mongodb.org/mongo-driver/bson"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetEquipmentLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetEquipmentLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetEquipmentLogic {
	return &GetEquipmentLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetEquipmentLogic) GetEquipment(in *dataCenter.GetEquipmentRequest) (*dataCenter.GetEquipmentResponse, error) {
	filter := bson.M{"DeviceID": in.DeviceID}

	one, err := databases.FindAny(context.Background(), filter, consts.Address)

	info, err := json.Marshal(one)

	if err != nil {
		return nil, err
	}

	return &dataCenter.GetEquipmentResponse{
		Info: info,
	}, nil
}
