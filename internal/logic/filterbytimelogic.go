package logic

import (
	"context"
	"encoding/json"
	"gitee.com/maoyuting0503109/fraework/databases"
	"gitee.com/zhangsan7774/data-centers/consts"
	"gitee.com/zhangsan7774/data-centers/dataCenter"
	"gitee.com/zhangsan7774/data-centers/internal/svc"
	"go.mongodb.org/mongo-driver/bson"
	"time"

	"github.com/zeromicro/go-zero/core/logx"
)

type FilterByTimeLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewFilterByTimeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *FilterByTimeLogic {
	return &FilterByTimeLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *FilterByTimeLogic) QueryByTime(in *dataCenter.FilterByTimeRequest) ([]bson.D, error) {
	startTime, err := time.Parse(time.DateTime, in.StartTime)
	if err != nil {
		return nil, err
	}

	endTime, err := time.Parse(time.DateTime, in.EndTime)
	if err != nil {
		return nil, err
	}
	filter := bson.M{
		"DeviceID": in.DeviceID,
		"Timestamp": bson.M{
			"$gte": startTime.String(),
			"$lte": endTime.String(),
		},
	}

	data, err := databases.FindAny(context.Background(), filter, consts.Address)

	if err != nil {
		return nil, err
	}

	return data, nil
}

func (l *FilterByTimeLogic) FilterByTime(in *dataCenter.FilterByTimeRequest) (*dataCenter.FilterByTimeResponse, error) {
	data, err := l.QueryByTime(in)

	if err != nil {
		return nil, err
	}

	info, err := json.Marshal(data)

	if err != nil {
		return nil, err
	}

	return &dataCenter.FilterByTimeResponse{Info: info}, nil
}
