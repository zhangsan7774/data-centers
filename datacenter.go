package main

import (
	"flag"
	config2 "gitee.com/maoyuting0503109/fraework/config"
	"gitee.com/maoyuting0503109/fraework/emqx"
	"gitee.com/zhangsan7774/data-centers/consts"
	"gitee.com/zhangsan7774/data-centers/dataCenter"
	"gitee.com/zhangsan7774/data-centers/internal/ReceivedData"
	"gitee.com/zhangsan7774/data-centers/internal/config"
	"gitee.com/zhangsan7774/data-centers/internal/server"
	"gitee.com/zhangsan7774/data-centers/internal/svc"
	"github.com/spf13/viper"
	"github.com/zeromicro/go-zero/core/conf"
	"log"

	"github.com/zeromicro/go-zero/core/service"
	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	flag.Parse()

	var c config.Config

	err := config2.ViperInit(consts.Address)
	if err != nil {
		panic(err)
	}

	err = config2.ClientNacos(viper.GetString("Nacos.Name"), viper.GetString("Nacos.Host"), viper.GetInt("Nacos.Port"))
	if err != nil {
		panic(err)
	}

	getConfig, err := config2.GetConfig(viper.GetString("Nacos.DataID"), viper.GetString("Nacos.Group"))
	if err != nil {
		panic(err)
	}

	err = conf.LoadFromYamlBytes([]byte(getConfig), &c)
	if err != nil {
		panic(err)
	}

	ctx := svc.NewServiceContext(c)

	s := zrpc.MustNewServer(c.RpcServerConf, func(grpcServer *grpc.Server) {
		dataCenter.RegisterDataCenterServer(grpcServer, server.NewDataCenterServer(ctx))
		if c.Mode == service.DevMode || c.Mode == service.TestMode {
			reflection.Register(grpcServer)
		}
	})

	defer s.Stop()

	client, errs := emqx.CreateMqttClient(consts.Address)
	if errs != nil {
		panic(err)
	}
	//mqtt 接收数据
	go ReceivedData.ReceivedData(c, client)

	log.Printf("Starting rpc server at %s...\n", c.ListenOn)
	s.Start()
}
